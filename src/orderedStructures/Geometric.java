package orderedStructures;

public class Geometric extends Progression {

	private double commonFactor; 
	
	public Geometric(double firstValue, double commonFactor) { 
		super(firstValue); 
		this.commonFactor = commonFactor; 
	}
	
	@Override
	public double nextValue() {
		if(!trip){
			throw new IllegalStateException("First value!");
		}
		current = current * commonFactor; 
		return current;
	}
	
	public String toString(){
		return "Geom("+ (int) this.firstValue()+ "," + (int) commonFactor+ ")";
	}
	
	@Override
	public double getTerm(int n){
		double term=firstValue()*(Math.pow(this.commonFactor, n-1));
		return term;
	}
}
